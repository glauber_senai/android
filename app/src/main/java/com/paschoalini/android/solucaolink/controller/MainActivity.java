package com.paschoalini.android.solucaolink.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.paschoalini.android.solucaolink.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();

        TextView link = (TextView) findViewById(R.id.txtEsqueci);

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Acesse nossa página na internet.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void acessarClick(View view) {
        String email = ((EditText) findViewById(R.id.txtEmail)).getText().toString();
        Integer senha = Integer.parseInt(((EditText) findViewById(R.id.txtSenha)).getText().toString());

        if(email.equals("glauber.paschoalini.senai@outlook.com") && senha == 123456) {
            Intent listagem = new Intent(this, ListagemActivity.class);
            startActivity(listagem);
        }
    }
}
