package com.paschoalini.android.solucaolink.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.paschoalini.android.solucaolink.R;
import com.paschoalini.android.solucaolink.dao.DaoAniversariante;
import com.paschoalini.android.solucaolink.model.Aniversariante;

public class ListagemActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem);

        DaoAniversariante bdNiver = new DaoAniversariante(this);

        ListView listagem = (ListView) findViewById(R.id.listagem);
        ArrayAdapter<Aniversariante> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, bdNiver.findAll());
        listagem.setAdapter(adaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.menu_novo:
                intent = new Intent(this, NovoActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_listar:
                intent = new Intent(this, ListagemActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }
}
