package com.paschoalini.android.solucaolink.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.paschoalini.android.solucaolink.model.Aniversariante;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DaoAniversariante extends SQLiteOpenHelper {
    private static final String NOME_DO_BANCO = "aniversarios";
    private static final int VERSAO_DO_BANCO = 1;
    private SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");

    public DaoAniversariante(Context context) {
        super(context, NOME_DO_BANCO, null, VERSAO_DO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists aniversarios (_id integer primary key autoincrement, nome text not null, nascimento text not null);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Durante o desenvolvimento, alteramos a versão do banco várias vezes
        db.execSQL("drop table if exists aniversarios;");
        db.execSQL("create table if not exists aniversarios (_id integer primary key autoincrement, nome text not null, nascimento text not null);");
    }

    public void salvar(Aniversariante aniversariante) {
        SQLiteDatabase bdNiver = getWritableDatabase();

        try {
            ContentValues valores = new ContentValues();
            valores.put("nome", aniversariante.getNome());
            valores.put("nascimento", formatador.format(aniversariante.getNascimento().getTimeInMillis()));

            if(aniversariante.getId() == 0) {
                // Parâmetros
                //      String com nome tabela (Obrigatório)
                //      String nome das colunas para gravar null caso ContentValues esteja vazio (não é obrigatório)
                //      ContentValues com os nomes das colunas e seus respectivos valores (Obrigatório)
                aniversariante.setId((int) bdNiver.insert("aniversarios", null, valores));
            } else {
                String[] dadosDaClausulaWhere = new String[]{String.valueOf(aniversariante.getId())};
                // update aniversarios set (colunas = valores) where _id=?
                bdNiver.update("aniversarios", valores, "_id=", dadosDaClausulaWhere);
            }
        } finally {
            bdNiver.close();
        }
    }

    public List<Aniversariante> findAll() {
        SQLiteDatabase bdNiver = getWritableDatabase();
        List<Aniversariante> listagem = new ArrayList<>();

        try {
            Cursor dados = bdNiver.query("aniversarios", null, null, null, null, null, "nome");

            if(dados.moveToFirst()) while (dados.moveToNext()) {
                Calendar aNascimento = Calendar.getInstance();
                String strData = dados.getString(dados.getColumnIndex("nascimento"));
                try {
                    aNascimento.setTimeInMillis(formatador.parse(strData).getTime());
                } catch (ParseException erro) {
                    Log.e("Glauber", erro.getMessage());
                }

                Aniversariante a = new Aniversariante();
                a.setId(dados.getInt(dados.getColumnIndex("_id")));
                a.setNome(dados.getString(dados.getColumnIndex("nome")));
                a.setNascimento(aNascimento);

                listagem.add(a);
            }
        } finally {
            bdNiver.close();
        }

        return listagem;
    }
}
