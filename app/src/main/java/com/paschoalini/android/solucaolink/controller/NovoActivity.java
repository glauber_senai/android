package com.paschoalini.android.solucaolink.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.paschoalini.android.solucaolink.R;
import com.paschoalini.android.solucaolink.dao.DaoAniversariante;
import com.paschoalini.android.solucaolink.model.Aniversariante;

import java.util.Calendar;

public class NovoActivity extends AppCompatActivity {
    private DatePicker leitorData;
    private EditText leitorNome;
    private DaoAniversariante bdNiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);

        bdNiver = new DaoAniversariante(this);

        leitorData = (DatePicker) findViewById(R.id.txtNascimento);
        leitorData.setMaxDate(Calendar.getInstance().getTimeInMillis());

        leitorNome = (EditText) findViewById(R.id.txtNome);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.principal_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.menu_novo:
                intent = new Intent(this, NovoActivity.class);
                startActivity(intent);
                break;
            case R.id.menu_listar:
                intent = new Intent(this, ListagemActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }

    public void gravarClick(View view) {
        if(leitorNome.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Quem nasceu nesta data?", Toast.LENGTH_SHORT).show();
        } else {
            String nome = leitorNome.getText().toString().trim();

            Calendar nascimento = Calendar.getInstance();
            nascimento.set(leitorData.getYear(), leitorData.getMonth(), leitorData.getDayOfMonth());

            Aniversariante a = new Aniversariante(nome, nascimento);
            bdNiver.salvar(a);

            if(a.getId() > 0)
                for(Aniversariante p : bdNiver.findAll())
                    Log.d("Glauber", p.toString());
        }
    }
}
