package com.paschoalini.android.solucaolink.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Aniversariante {
    private int id;
    private String nome;
    private Calendar nascimento;

    public Aniversariante() {

    }

    public Aniversariante(String nome, Calendar nascimento) {
        this.nome = nome;
        this.nascimento = nascimento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Calendar getNascimento() {
        return nascimento;
    }

    public void setNascimento(Calendar nascimento) {
        this.nascimento = nascimento;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return "Aniversariante{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", nascimento=" + sdf.format(nascimento.getTimeInMillis()) +
                '}';
    }
}
